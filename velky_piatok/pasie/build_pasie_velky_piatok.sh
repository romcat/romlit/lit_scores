#!/bin/bash
filename="pasie_velky_piatok"
out_dir="out_$filename"
mkdir -p $out_dir

cwd="$PWD"

if [ -d $out_dir ]; then
	lilypond-book --output=$out_dir --pdf --latex-program=xelatex pasie_velky_piatok.lytex && cd $out_dir && xelatex pasie_velky_piatok.tex
else
	lilypond-book --output=$out_dir --pdf --latex-program=xelatex pasie_velky_piatok.lytex && cd $out_dir && xelatex pasie_velky_piatok.tex && xelatex pasie_velky_piatok.tex
fi

cp pasie_velky_piatok.pdf ../
cd "$cwd"
unset filename