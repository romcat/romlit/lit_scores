### Vision
#### or a TODO list

There is no ETA set for anything. Once an item is done, it is removed from here and moved to [`changelog.md`](changelog.md). Note that anything in this list might change anytime without prior notification.

#### Near Future

- translate all repo root text files into Slovak (`*.sk.md`)
- add `code_of_conduct.md`
- add `authors.md` (something like [this](https://github.com/sinatra/sinatra/blob/master/AUTHORS.md))

#### Further Future

- add `version` (as discussed at `romcal` repo [here](https://github.com/romcal/romcal/issues/60))
- add `style_guide`
- use GitLab CI to generate new version of LaTeX documents
- create a logo/icon